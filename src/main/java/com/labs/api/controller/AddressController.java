package com.labs.api.controller;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.labs.api.annotations.PositiveNumber;
import com.labs.api.dto.ResponseBodyDTO;
import com.labs.api.service.AddressService;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@Validated
@Slf4j
public class AddressController extends BaseController {

	@Autowired
	private AddressService service;

	@GetMapping("/zipcode/{zipcode}")
	@ApiOperation("Busca endereço por CEP")
	public ResponseEntity getAddress(
			@PathVariable("zipcode") @NotBlank @Size(min = 8, max = 8) @PositiveNumber String zipcode) {

		AddressController.log.info(String.format("%s %s %s..", "get address by zipcode", zipcode, "starting"));
		
		ResponseBodyDTO response = this.service.getAddress(zipcode);

		AddressController.log.info(String.format("%s %s %s..", "get address by zipcode", zipcode, "finished"));
		
		return response.isSucess() ? buildResponse(response, HttpStatus.OK) : errorResponse(response);

	}

}

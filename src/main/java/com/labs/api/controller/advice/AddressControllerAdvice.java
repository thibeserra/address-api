package com.labs.api.controller.advice;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.labs.api.dto.ResponseBodyDTO;
import com.labs.api.factory.ErrorDTOFactory;

@ControllerAdvice
public class AddressControllerAdvice {

	@ExceptionHandler({ ConstraintViolationException.class })
	public ResponseEntity invalidArgumentController() {

		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ResponseBodyDTO.with(ErrorDTOFactory.getInvalidZipCode("zipcode")));

	}

}

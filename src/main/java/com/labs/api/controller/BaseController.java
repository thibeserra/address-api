package com.labs.api.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.labs.api.dto.ErrorDTO;
import com.labs.api.dto.ResponseBodyDTO;
import com.labs.api.enums.ErrorCodeEnum;

public abstract class BaseController {

	protected ResponseEntity buildResponse(ResponseBodyDTO responseBody, HttpStatus httpStatusSuccess) {
		return responseBody.isSucess() ? ResponseEntity.status(httpStatusSuccess).body(responseBody)
				: errorResponse(responseBody);
	}

	protected ResponseEntity errorResponse(ResponseBodyDTO responseBody) {
		return ResponseEntity
				.status(ErrorCodeEnum.findHttpStatus(((ErrorDTO) responseBody.getErrors().get(0)).getErrorCode()))
				.body(responseBody);
	}

}

package com.labs.api.service;

import com.labs.api.dto.ResponseBodyDTO;

public interface AddressService {

	public ResponseBodyDTO getAddress(String zipcode);
	
}

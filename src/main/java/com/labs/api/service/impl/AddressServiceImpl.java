package com.labs.api.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.labs.api.client.ViaCepApiClient;
import com.labs.api.dto.AddressResponseDTO;
import com.labs.api.dto.ResponseBodyDTO;
import com.labs.api.factory.ErrorDTOFactory;
import com.labs.api.service.AddressService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AddressServiceImpl implements AddressService {

	private static final int ZIPCODE_INDEX_SIZE = 7;
	
	@Autowired
	private ViaCepApiClient viaCepApiClient;

	@Override
	public ResponseBodyDTO getAddress(String zipcode) {
		
		return this.searchAddress(zipcode);
	}

	private ResponseBodyDTO searchAddress(String zipcode) {

		ResponseBodyDTO response = new ResponseBodyDTO<>();
		AddressResponseDTO responseBody = null;

		try {
			for (int zipcodeIndex = ZIPCODE_INDEX_SIZE; zipcodeIndex >= 0; zipcodeIndex--) {
				
				AddressServiceImpl.log.info(String.format("%s %s", "getting address to zipcode", zipcode));
				
				responseBody = (AddressResponseDTO) this.viaCepApiClient.getAddress(zipcode).getBody();

				if (this.getSucess(responseBody)) {
					response.addRecord(responseBody);
					
					return response;
				}

				if (this.processedAllAlternatives(zipcodeIndex)) {
					AddressServiceImpl.log.warn(String.format("%s. %s %s", "warn on get address", "address not found to zipcode", zipcode));
					response.addError(ErrorDTOFactory.getNotFound("address"));
				}
				else zipcode = this.editZipCode(zipcode, Character.forDigit(0, 10), zipcodeIndex);
				
			}
		} catch (Exception e) {
			
			AddressServiceImpl.log.error(String.format("%s %s %s: %s", "fail on get address to cep", zipcode,
					"from viacep api. Error ", e.getMessage()));
			
			response.addError(ErrorDTOFactory.getInternalServerError("error exception on get address by zipcode"));
		}

		return response;
	}

	
	private String editZipCode(String zipcode, char charValue, int index) {

		if (String.valueOf(zipcode.charAt(index)).equals("0")) return zipcode;

		char[] zipcodeChar = zipcode.toCharArray();
		zipcodeChar[index] = charValue;

		return String.valueOf(zipcodeChar);

	}

	
	private boolean getSucess(AddressResponseDTO responseBody) {
		return !responseBody.isError();
	}
	
	
	private boolean processedAllAlternatives(int zipcodeIndex) {
		return zipcodeIndex == 0;
	}
}

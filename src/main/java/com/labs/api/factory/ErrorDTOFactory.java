package com.labs.api.factory;

import com.labs.api.dto.ErrorDTO;
import com.labs.api.enums.ErrorCodeEnum;

public class ErrorDTOFactory {

	private ErrorDTOFactory() {}
	
	public static ErrorDTO getNotFound(String name) {

		return new ErrorDTO(String.format("%s %s", name, "not found"),
				String.format("%s %s, %s", "You attempted to get a", name, "but did not find any"),
				ErrorCodeEnum.NOT_FOUND.getCode());
	}

	public static ErrorDTO getInternalServerError(String message) {
		
		return new ErrorDTO(message, "Sorry, something went wrong", ErrorCodeEnum.INTERNAL_SERVER_ERROR.getCode());
	}

	public static ErrorDTO getUnAuthorized() {
		
		return new ErrorDTO("Unauthorized - make sure the header parameter Authorization is valid",
				"You are not authorized to perform this operation", ErrorCodeEnum.UNAUTHORIZED.getCode());
	}

	public static ErrorDTO getInvalidZipCode(String field) {
		
		return new ErrorDTO(String.format("%s %s %s", "Invalid", field, "value"),
				String.format("%s %s %s", "Invalid", field, "value"), ErrorCodeEnum.INVALID_ZIP_CODE.getCode());
	}

}

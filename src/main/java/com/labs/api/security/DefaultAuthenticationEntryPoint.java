package com.labs.api.security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import com.google.gson.Gson;
import com.labs.api.dto.ResponseBodyDTO;
import com.labs.api.factory.ErrorDTOFactory;


public class DefaultAuthenticationEntryPoint implements AuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
		
		response.setStatus(401);
		response.setContentType("application/json");
		response.getWriter().append(buildUnAuthorizedResponse());
	}

	private String buildUnAuthorizedResponse() {
		return new Gson().toJson(new ResponseBodyDTO<>(ErrorDTOFactory.getUnAuthorized()));
	}
}

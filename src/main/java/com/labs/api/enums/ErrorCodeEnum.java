package com.labs.api.enums;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorCodeEnum {

	NOT_FOUND(20023L, HttpStatus.NOT_FOUND),
	INTERNAL_SERVER_ERROR(10000L, HttpStatus.INTERNAL_SERVER_ERROR),
	UNAUTHORIZED(30001L, HttpStatus.UNAUTHORIZED),
	INVALID_ZIP_CODE(20026l, HttpStatus.BAD_REQUEST);
	
	private Long code;
	
	private HttpStatus httpCode;
	
	public static HttpStatus findHttpStatus(Long errorCode) {
		
		for (ErrorCodeEnum error : ErrorCodeEnum.values()) {
			if(error.getCode().equals(errorCode)) {
				return error.getHttpCode();
			}
		}
		
		return null;
	}
	
}

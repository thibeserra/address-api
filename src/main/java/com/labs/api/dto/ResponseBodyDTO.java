package com.labs.api.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ResponseBodyDTO<T> {

	private List<ErrorDTO> errors;
	private List<T> records;
	
	public ResponseBodyDTO(ErrorDTO error ) {
		if(this.errors == null ) {
			this.errors = new ArrayList<>();
		}
		this.errors.add(error);
	}

	public void addError(ErrorDTO error) {
		
		if (errors == null) {
			errors = new ArrayList<>();
		}
		errors.add(error);
	}
	
	public void addRecord(T record) {
		
		if (this.records == null) {
			records = new ArrayList<>();
		}

		records.add(record);
	}

	public static ResponseBodyDTO with(ErrorDTO error) {
		
		ResponseBodyDTO response = new ResponseBodyDTO<>();
		response.addError(error);
		return response;
	}

	public boolean isSucess() {
		
		return errors == null || errors.isEmpty();
	}
}
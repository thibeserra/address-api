package com.labs.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AddressResponseDTO {
	
	@JsonProperty("logradouro")
	private String street;
	
	@JsonProperty("bairro")
	private String neighborhood;
	
	@JsonProperty("localidade")
	private String city;
	
	private String uf;
	
	@JsonInclude(value = Include.NON_DEFAULT)
	@JsonProperty("erro")
	private boolean error;
	
}

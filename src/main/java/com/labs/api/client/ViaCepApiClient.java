package com.labs.api.client;

import org.springframework.http.ResponseEntity;

public interface ViaCepApiClient {

	ResponseEntity getAddress(String zipcode);
}

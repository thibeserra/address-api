package com.labs.api.client.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.labs.api.client.ViaCepApiClient;
import com.labs.api.client.ViaCepFeignClient;

@Component
public class ViaCepApiClientImpl implements ViaCepApiClient {

	@Autowired
	private ViaCepFeignClient api;
	
	@Override
	public ResponseEntity getAddress(String zipcode) {
		
		 return this.api.getAddress(zipcode);
		
	}
	
	
}

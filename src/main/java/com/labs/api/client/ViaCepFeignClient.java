package com.labs.api.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.labs.api.dto.AddressResponseDTO;

@FeignClient(value = "viacep",
             path = "/ws")
public interface ViaCepFeignClient {

	@GetMapping(value = "/{zipcode}/json")
	ResponseEntity<AddressResponseDTO> getAddress(@PathVariable("zipcode") String zipcode);
}

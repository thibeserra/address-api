package com.labs.api.config;

import org.springframework.beans.factory.annotation.Value;

import lombok.Getter;

@Getter
public class JwtConfig {
	
	@Value("${security.jwt.uri:/**}")
	private String uri;

	@Value("${security.jwt.header:Authorization}")
	private String header;

	@Value("${security.jwt.prefix:Bearer}")
	private String prefix;

	@Value("${security.jwt.secret}")
	private String secret;

}


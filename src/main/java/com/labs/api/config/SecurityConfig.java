package com.labs.api.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import com.labs.api.security.DefaultAuthenticationEntryPoint;
import com.labs.api.security.JwtAuthenticationFilter;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtConfig jwtConfig;

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.csrf().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
		.exceptionHandling().authenticationEntryPoint(new DefaultAuthenticationEntryPoint()).and()
		.authorizeRequests().antMatchers("/actuator/**", "/admin/**", "/swagger-resources/**", "/v2/api-docs/**", "/csrf/**",
				"/webjars/**", "/swagger-ui.html")
		.permitAll().anyRequest().authenticated();
		http.addFilter(new JwtAuthenticationFilter(authenticationManager(), jwtConfig));

	}

	@Bean
	public JwtConfig jwtConfig() {
		return new JwtConfig();
	}
}

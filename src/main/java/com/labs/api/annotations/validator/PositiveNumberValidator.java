package com.labs.api.annotations.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.labs.api.annotations.PositiveNumber;

public class PositiveNumberValidator implements ConstraintValidator<PositiveNumber, String> {

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		
		return value.matches("[0-9]+");
	}

}

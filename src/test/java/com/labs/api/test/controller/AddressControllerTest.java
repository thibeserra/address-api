package com.labs.api.test.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.labs.api.controller.AddressController;
import com.labs.api.dto.AddressResponseDTO;
import com.labs.api.dto.ResponseBodyDTO;
import com.labs.api.factory.ErrorDTOFactory;
import com.labs.api.service.AddressService;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = { AddressController.class })
public class AddressControllerTest	 {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private AddressService service;
	
	@Value("${security.jwt.token-test}")
	private String tokenTest;
	
	
	@DisplayName("when get address by zipcode with value 14403383 then get 200 HttpStatus")
	@Test
	public void whenGetAddressByZipcodeWithValue14403383_then_Get200HttpStatus() throws Exception {
		
		ResponseBodyDTO<AddressResponseDTO> responseServiceMock = new ResponseBodyDTO<>();
		responseServiceMock.addRecord(AddressResponseDTO
										.builder()
											.uf("SP")
											.street("Rua Primo Tasso")
											.neighborhood("Vila Santa Rita")
											.city("Franca")
										.build());
		
		Mockito.when(this.service.getAddress("14403383")).thenReturn(responseServiceMock);
		
		this.mockMvc.perform(get("/zipcode/{zipcode}", "14403383")
				.header("authorization", this.tokenTest)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.sucess").value(true))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors").doesNotExist())
				.andExpect(MockMvcResultMatchers.jsonPath("$.records").exists())
				.andExpect(MockMvcResultMatchers.jsonPath("$.records[0].uf").value("SP"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.records[0].logradouro").value("Rua Primo Tasso"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.records[0].bairro").value("Vila Santa Rita"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.records[0].localidade").value("Franca"));
		
	}
	
	@DisplayName("when get address by zipcode with value 14403383 then get 500 HttpStatus")
	@Test
	public void whenGetAddressByZipcodeWithValue14403383_then_Get500HttpStatus() throws Exception {
		
		Mockito.when(this.service.getAddress("14403383")).thenReturn(ResponseBodyDTO
				.with(ErrorDTOFactory.getInternalServerError("error exception on get address by zipcode")));
		
		this.mockMvc.perform(get("/zipcode/{zipcode}", "14403383")
				.header("authorization", this.tokenTest)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isInternalServerError())
				.andExpect(MockMvcResultMatchers.jsonPath("$.sucess").value(false))
				.andExpect(MockMvcResultMatchers.jsonPath("$.records").doesNotExist())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].developerMessage")
						.value("error exception on get address by zipcode"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].userMessage")
						.value("Sorry, something went wrong"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].errorCode").value(10000L));
		
	}
	
	@DisplayName("when get address by zipcode with invalid value 1440338 then get 400 HttpStatus")
	@Test
	public void whenGetAddressByZipcodeWithInvalidValue1440338_then_Get400HttpStatus() throws Exception {
		
		Mockito.when(this.service.getAddress("1440338")).thenReturn(ResponseBodyDTO
				.with(ErrorDTOFactory.getInvalidZipCode("zipcode")));
		
		this.mockMvc.perform(get("/zipcode/{zipcode}", "1440338")
				.header("authorization", this.tokenTest)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
				.andExpect(MockMvcResultMatchers.jsonPath("$.sucess").value(false))
				.andExpect(MockMvcResultMatchers.jsonPath("$.records").doesNotExist())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].developerMessage")
						.value("Invalid zipcode value"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].userMessage")
						.value("Invalid zipcode value"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].errorCode").value(20026L));
		
	}
	
	@DisplayName("when get address by zipcode with negative value then get 400 HttpStatus")
	@Test
	public void whenGetAddressByZipcodeWithNegativeValue_then_Get400HttpStatus() throws Exception {
		
		this.mockMvc.perform(get("/zipcode/{zipcode}", "-1440338")
				.header("authorization", this.tokenTest)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
				.andExpect(MockMvcResultMatchers.jsonPath("$.sucess").value(false))
				.andExpect(MockMvcResultMatchers.jsonPath("$.records").doesNotExist())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].developerMessage")
						.value("Invalid zipcode value"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].userMessage")
						.value("Invalid zipcode value"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].errorCode").value(20026L));
		
	}
	
	@DisplayName("when get address by zipcode with character value then get 400 HttpStatus")
	@Test
	public void whenGetAddressByZipcodeWithCharacterValue_then_Get400HttpStatus() throws Exception {
		
		this.mockMvc.perform(get("/zipcode/{zipcode}", "aaaaaaaa")
				.header("authorization", this.tokenTest)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
				.andExpect(MockMvcResultMatchers.jsonPath("$.sucess").value(false))
				.andExpect(MockMvcResultMatchers.jsonPath("$.records").doesNotExist())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors").exists())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].developerMessage")
						.value("Invalid zipcode value"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].userMessage")
						.value("Invalid zipcode value"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].errorCode").value(20026L));
		
	}
	
	@DisplayName("when get address by zipcode with empty value then get 400 HttpStatus")
	@Test
	public void whenGetAddressByZipcodeWithEmptyValue_then_Get400HttpStatus() throws Exception {
		
		this.mockMvc.perform(get("/zipcode/{zipcode}", " ")
				.header("authorization", this.tokenTest)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
				.andExpect(MockMvcResultMatchers.jsonPath("$.sucess").value(false))
				.andExpect(MockMvcResultMatchers.jsonPath("$.records").doesNotExist())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors").exists())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].developerMessage")
						.value("Invalid zipcode value"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].userMessage")
						.value("Invalid zipcode value"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].errorCode").value(20026L));
		
	}
	
	@DisplayName("when get address by zipcode with value 10000000 then get 404 HttpStatus")
	@Test
	public void whenGetAddressByZipcodeWithValue10000000_then_Get404HttpStatus() throws Exception {
		
		Mockito.when(this.service.getAddress("10000000")).thenReturn(ResponseBodyDTO
				.with(ErrorDTOFactory.getNotFound("address not found")));
		
		this.mockMvc.perform(get("/zipcode/{zipcode}", "10000000")
				.header("authorization", this.tokenTest)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound())
				.andExpect(MockMvcResultMatchers.jsonPath("$.sucess").value(false))
				.andExpect(MockMvcResultMatchers.jsonPath("$.records").doesNotExist())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors").exists())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].userMessage")
						.value("You attempted to get a address not found, but did not find any"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].errorCode").value(20023L));
	}
	
	@DisplayName("when get address by zipcode with value 14403383 without authentication then get 401 HttpStatus")
	@Test
	public void whenGetAddressByZipcodeWithValue14403383WithoutAuthentication_then_Get401HttpStatus() throws Exception {
		
		this.mockMvc.perform(get("/zipcode/{zipcode}", "14403383")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized())
				.andExpect(MockMvcResultMatchers.jsonPath("$.records").doesNotExist())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors").exists())
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].developerMessage")
						.value("Unauthorized - make sure the header parameter Authorization is valid"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].userMessage")
						.value("You are not authorized to perform this operation"))
				.andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].errorCode").value(30001L));
		
	}
	
}

package com.labs.api.test.service;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.labs.api.client.ViaCepApiClient;
import com.labs.api.dto.AddressResponseDTO;
import com.labs.api.dto.ErrorDTO;
import com.labs.api.dto.ResponseBodyDTO;
import com.labs.api.service.AddressService;
import com.labs.api.service.impl.AddressServiceImpl;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { AddressServiceImpl.class })
public class AddressServiceTest {

	@Autowired
	private AddressService service;

	@MockBean
	private ViaCepApiClient apiClient;

	@DisplayName("when get address by zipcode with value 14403383 then get address with sucess")
	@Test
	public void whenGetAddressByZipcodeWithValue14403383_then_GetAddress() {

		AddressResponseDTO apiBodyResponseMock = 
				AddressResponseDTO
				.builder()
					.street("Rua Primo Tasso")
					.neighborhood("Vila Santa Rita")
					.city("Franca")
					.uf("SP")
					.error(false)
				.build();

		ResponseEntity<AddressResponseDTO> apiResponseMock = new ResponseEntity<AddressResponseDTO>(apiBodyResponseMock,
				HttpStatus.OK);

		Mockito.when(this.apiClient.getAddress("14403383")).thenReturn(apiResponseMock);

		ResponseBodyDTO response = this.service.getAddress("14403383");

		Assertions.assertNotNull(response);
		Assertions.assertNull(response.getErrors());
		Assertions.assertNotNull(response.getRecords());
		Assertions.assertEquals(1, response.getRecords().size());
		List<AddressResponseDTO> responseBody = response.getRecords();
		Assertions.assertEquals("SP", responseBody.get(0).getUf());
		Assertions.assertEquals("Franca", responseBody.get(0).getCity());
		Assertions.assertEquals("Vila Santa Rita", responseBody.get(0).getNeighborhood());
		Assertions.assertEquals("Rua Primo Tasso", responseBody.get(0).getStreet());

	}
	
	@DisplayName("when get address by zipcode with value 11000000 then not found address")
	@Test
	public void whenGetAddressByZipcodeWithValue11000000_then_NotFoundAddress() {

		AddressResponseDTO apiBodyResponseMock = 
				AddressResponseDTO
				.builder()
					.error(true)
				.build();

		ResponseEntity<AddressResponseDTO> apiResponseMock = new ResponseEntity<AddressResponseDTO>(apiBodyResponseMock,
				HttpStatus.OK);

		Mockito.when(this.apiClient.getAddress("11000000")).thenReturn(apiResponseMock);
		Mockito.when(this.apiClient.getAddress("10000000")).thenReturn(apiResponseMock);

		ResponseBodyDTO response = this.service.getAddress("11000000");

		Assertions.assertNotNull(response);
		Assertions.assertNotNull(response.getErrors());
		Assertions.assertNull(response.getRecords());
		Assertions.assertEquals(1, response.getErrors().size());
		ErrorDTO error = (ErrorDTO) response.getErrors().get(0);
		Assertions.assertEquals("address not found", error.getDeveloperMessage());
		Assertions.assertEquals("You attempted to get a address, but did not find any", error.getUserMessage());
		Assertions.assertEquals(20023L, error.getErrorCode());

	}
	
	@DisplayName("when get address by zipcode with any value then throw exception from api client")
	@Test
	public void whenGetAddressByZipcodeWithAnyValue_then_ThrowExceptionFromApiClient() {
		
		Mockito.when(this.apiClient.getAddress(Mockito.anyString())).thenThrow(RuntimeException.class);

		ResponseBodyDTO response = this.service.getAddress("11000000");

		Assertions.assertNotNull(response);
		Assertions.assertNotNull(response.getErrors());
		Assertions.assertNull(response.getRecords());
		Assertions.assertEquals(1, response.getErrors().size());
		ErrorDTO error = (ErrorDTO) response.getErrors().get(0);
		Assertions.assertEquals("error exception on get address by zipcode", error.getDeveloperMessage());
		Assertions.assertEquals("Sorry, something went wrong", error.getUserMessage());
		Assertions.assertEquals(10000L, error.getErrorCode());

	}

}

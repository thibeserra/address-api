package com.labs.api.test.client;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.labs.api.client.ViaCepApiClient;
import com.labs.api.client.ViaCepFeignClient;
import com.labs.api.client.impl.ViaCepApiClientImpl;
import com.labs.api.dto.AddressResponseDTO;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { ViaCepApiClientImpl.class })
public class ViaCepApiClientTest {


	@Autowired
	private ViaCepApiClient apiClient;
	
	@MockBean
	private ViaCepFeignClient feignClient;
	
	@DisplayName("when request client address by zipcode with value 14403383 then get address with 200 HttpStatus")
	@Test
	public void whenRequestClientAddressByZipcodeWithValue14403383_then_GetAddress() {
		
		AddressResponseDTO apiBodyResponseMock = 
				AddressResponseDTO
				.builder()
					.street("Rua Primo Tasso")
					.neighborhood("Vila Santa Rita")
					.city("Franca")
					.uf("SP")
					.error(false)
				.build();

		ResponseEntity<AddressResponseDTO> apiResponseMock = new ResponseEntity<AddressResponseDTO>(apiBodyResponseMock,
				HttpStatus.OK);
		
		Mockito.when(this.feignClient.getAddress("14403383")).thenReturn(apiResponseMock);
		
		ResponseEntity apiResponse = this.apiClient.getAddress("14403383");
		
		Assertions.assertNotNull(apiResponse);
		Assertions.assertEquals(HttpStatus.OK, apiResponse.getStatusCode());
		Assertions.assertNotNull(apiResponse.getBody());
		AddressResponseDTO bodyResponse = (AddressResponseDTO) apiResponse.getBody();
		Assertions.assertEquals("SP", bodyResponse.getUf());
		Assertions.assertEquals("Franca", bodyResponse.getCity());
		Assertions.assertEquals("Vila Santa Rita", bodyResponse.getNeighborhood());
		Assertions.assertEquals("Rua Primo Tasso", bodyResponse.getStreet());
		
	}
	
	
}

[![CircleCI](https://circleci.com/bb/thibeserra/address-api/tree/master.svg?style=svg)](https://app.circleci.com/pipelines/bitbucket/thibeserra/address-api?branch=master)


# Address API

O Address API tem como objetivo expor serviços de busca de endereços de uma determinada cidade/bairro/região baseado no CEP informado pelo cliente do serviço. Todos os endpoints da API são autenticados por token JWT. Para obter mais detalhes, acesse o [Confluence](https://thiagobeserra.atlassian.net/wiki/spaces/AA/overview) da API.



## Tecnologias utilizadas

[Java JDK 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html) como plataforma e linguagem de programação.

[Maven 3.6.3](https://maven.apache.org/download.cgi) para gerenciamento de build, test e compilação.

[Spring Boot 2.3.3.RELEASE](https://spring.io/projects/spring-boot) como template Spring.

[Spring Boot Admin 2.3.0](https://github.com/codecentric/spring-boot-admin) para monitoria da saúde de aplicações Spring.

[Swagger 2.9.2](https://swagger.io) para documentação e consumo visual de recursos de API.

[Feign 2.2.2.RELEASE](https://cloud.spring.io/spring-cloud-openfeign/reference/html) como web service client.

[Sleuth with Zipkin 2.2.4.RELEASE](https://spring.io/projects/spring-cloud-sleuth) para registro de trace de logs distribuídos.

[JaCoCo 0.8.5](https://github.com/jacoco/jacoco) como plugin para gerar coverage da aplicação.

[SonarQube 3.6.0.1398](https://www.sonarqube.org) para métricas de qualidade e segurança da aplicação.

[CircleCI 2](https://circleci.com/) para CI (Continuous Integration) da aplicação.

[Docker Compose 1.26.0](https://docs.docker.com/compose/gettingstarted) para executar múltiplos containers Docker.



## Adoção do Java - Justificativa

A plataforma escolhida foi o [Java](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html), por ser consolidada e amadurecida no mercado, além de rica em frameworks e bibliotecas open source (*Spring e SpringBoot, por exemplo*) para construção de API's.
Outro motivo em especial foi pela *experiência* que [obtenho](https://www.linkedin.com/in/thiago-de-paula-beserra-2b174177/) na plataforma, se comparado à outras linguagens.
Como observação, não haveria *impedimentos* em construir a mesma solução em outra plataforma, como por exemplo [NodeJs](https://nodejs.org/en/).



## Requisitos

```sh
Java 11
Docker Compose
Plugin Lombok
```

**Lombok plugin:**
```sh
Intellij: https://projectlombok.org/setup/intellij
Eclipse : https://projectlombok.org/setup/eclipse
```



## Configuração para Desenvolvimento

Acessar a pasta raiz do projeto:

**Compilar o projeto:**
```sh
./mvnw clean package
```



#### Executar o projeto com configuração local:

```sh
sudo docker-compose up -d
./mvnw clean spring-boot:run -Dspring-boot.run.profiles=dev

```



#### Verificar o coverage:

**JaCoCo**
```sh
./mvnw clean install jacoco:report
```
Após executar o comando, será disponível o coverage em formato **html** na seguinte pasta do projeto:

```/target/site/jacoco/index.html```

**SonarQube**

***1*** - Com o projeto local e docker-compose em execução, acesse o [Dashboard](http://localhost:9000) do sonar. 

***2*** - realize o login *(usuario e senha padrão: admin)*. 

***3*** - crie uma aplicação, clicando no ícone '+', menu **analise new project** *(no canto superior direito)*. 

***4*** - informe o nome do token e clique no botão **generate**. *(sugestão: nome da aplicação, por exemplo - address-api)*. 

***5*** - clique no botão **continue**. 

***6*** - Escolha a opção **Java**. 

***7*** - Escolha a opção **Maven**. 

***8*** - Copie o comando *maven* que foi disponível na parte direita do modal *(obs: substitua **mvn** por **./mvnw** caso não possua o maven instalado localmente em seu ambiente)*. 

***9*** - clique no link **Finish tutorial** no canto inferior direito. 

***10*** - execute o comando:
```sh
./mvnw clean package
```

***11*** - execute o comando extraido no **passo 9**. Segue um exemplo:
```sh
./mvnw sonar:sonar \
  -Dsonar.host.url=http://localhost:9000 \
  -Dsonar.login=6039b1c1175ac5aa537618cb8fec56d104c8897f
```
***12*** - acesse o [Dashboard](http://localhost:9000) do sonar e clique no **projeto criado**. *(obs: Na aba **overview**, é possível verificar informações tais como, **bugs**, **code smells** e o nível de **coverage** do projeto. Também na aba **code**, é possível visualizar todo o código fonte do projeto e o nível  de **coverage** individual de cada um).*



#### Verificar logs de trace distribuído da API:

**Zipkin**

***1*** - Com o projeto local e docker-compose em execução, acesse o [Dashboard](http://localhost:9411) do zipkin. 

***2*** - No canto superior esquerdo, informe o intervalo de tempo e o limite de traces. 

***3*** - No canto superior direito, clique no botão **RUN QUERY** para realizar a pesquisa. 

***4*** - Caso exista log de traces no intervalo filtrado, será disponibilizado uma lista de traces com a informação do **nome do serviço(*root*)**, **trace(*trace id*)**, **data inicial(*start time*) da requisição** e **duração(*duration*) da requisição**. *(obs: Caso deseje verificar mais detalhes, clique na linha específica e analise o detalhe de cada requisição interna, tais como, rastro completo da requisição, método HTTP, path do serviço, nome da controller, método da controller invocado, dentre outras informações)*. 




#### Monitorar saúde da Aplicação:

**Spring Boot Admin**

***1*** - Com o projeto local e docker-compose em execução, acesse o [Spring Boot Admin](http://localhost:8080/address/api/admin).

***2*** - Na dashboard inicial, será informada a **quantidade de aplicações(*applications*)**, **instancias (*instances*)** e se a **aplicação está no ar (*status*)**.

***3*** - Na listagem de aplicações, clique na aplicação **Address API**.

***4*** - Na dashboard da aplicação, será disponibilizado várias informações da aplicação, tais como, **nome e versão**, **data e hora que a aplicação foi iniciada**, **health check (*Status UP ou DOWN*)**, informações de **espaço em disco**, **ping**, **id do processo da instancia** e informações de **consumo da CPU**, **garbage collector**, **consumo de threads**, **memória**, dentre outras informações.



## Conteúdo relevante

[O Protocolo HTTP](https://thiagobeserra.atlassian.net/wiki/spaces/AA/pages/33441/O+PROTOCOLO+HTTP) - Simples resumo do que é, onde é aplicado e como funciona (1 minuto de leitura aproximadamente).